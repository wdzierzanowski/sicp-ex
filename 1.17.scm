(define (mul b n)
  (cond ((= n 0) 0)
        ((even? n) (double (mul b (halve n))))
        (else (+ b (mul b (- n 1))))))

(define (double b)
  (* b 2))

(define (halve b)
  (/ b 2))

(mul 2 11)

(define (linmul b n)
  (linmul-iter 0 b n))

(define (linmul-iter a b n)
  (cond ((= n 0) a)
        ((even? n) (linmul-iter a (double b) (halve n)))
        (else (linmul-iter (+ a b) (double b) (halve (- n 1))))))

(linmul 2 11)
