(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))

(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (if (null? bits)
        '()
        (let ((next-branch
               (choose-branch (car bits) current-branch)))
          (if (leaf? next-branch)
              (cons (symbol-leaf next-branch)
                    (decode-1 (cdr bits) tree))
              (decode-1 (cdr bits) next-branch)))))
  (define (choose-branch bit branch)
    (cond ((= bit 0) (left-branch branch))
          ((= bit 1) (right-branch branch))
          (else (error "bad bit -- CHOOSE-BRANCH" bit))))
  (decode-1 bits tree))

; 2.67
(define sample-tree
  (make-code-tree (make-leaf 'A 4)
                  (make-code-tree
                   (make-leaf 'B 2)
                   (make-code-tree (make-leaf 'D 1)
                                   (make-leaf 'C 1)))))

(define sample-message '(0 1 1 0 0 1 0 1 0 1 1 1 0))

(decode sample-message sample-tree)

; 2.68
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol symbol tree)
  (if (leaf? tree)
      (if (eq? symbol (symbol-leaf tree))
          '()
          (error "no such symbol" symbol))
      (if (memq symbol (symbols (left-branch tree)))
          (cons 0 (encode-symbol symbol (left-branch tree)))
          (cons 1 (encode-symbol symbol (right-branch tree))))))

(encode (decode sample-message sample-tree) sample-tree)

; 2.69
(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)    ; symbol
                               (cadr pair))  ; frequency
                    (make-leaf-set (cdr pairs))))))

(define (generate-huffman-tree pairs)
  (define (successive-merge set)
    (if (eq? (length set) 1)
        (car set)
        (let ((first-smallest (car set))
              (second-smallest (cadr set))
              (rest-of-set (cddr set)))
          (let ((new-set (adjoin-set
                          (make-code-tree first-smallest second-smallest)
                          rest-of-set)))
            (successive-merge new-set)))))
  (successive-merge (make-leaf-set pairs)))

sample-tree
(define sample-tree (generate-huffman-tree '((A 4) (B 2) (C 1) (D 1))))
sample-tree
(encode (decode sample-message sample-tree) sample-tree)

; 2.70
(define rock-tree
  (generate-huffman-tree
   '((na 16) (yip 9) (sha 3) (a 2) (get 2) (job 2) (boom 1) (wah 1))))
(define decoded '(get a job sha na na na na na na na na get a job sha na na na na na na na na wah yip yip yip yip yip yip yip yip yip sha boom))
decoded
(define encoded (encode decoded rock-tree))
encoded
; How many bits are required for the encoding?
(length encoded)
; What is the smallest number of bits that would be needed to encode this song
; if we used a fixed-length code for the eight-symbol alphabet?
(* (length decoded) 3)

; 2.71
(define tree5
  (generate-huffman-tree
    '((s1 1) (s2 2) (s3 4) (s4 8) (s5 16))))
tree5
;
;       /\
;      /\ s5
;     /\ s4
;   /\  s3
; s1  s2 

(define tree10
  (generate-huffman-tree
    '((s1 1) (s2 2) (s3 4) (s4 8) (s5 16) (s6 32) (s7 64) (s8 128) (s9 256) (s10 512))))
tree10

; How many bits are required to encode the most frequent symbol?
1
; the least frequent symbol?
n - 1

; 2.72
;
;give the order of growth (as a function of n) of the number of steps needed
;to encode the most frequent
;   O(n) because we need to search through the (n-1) symbols in the left branch
;   and the 1 symbol in the right branch
;
; and least frequent symbols in the alphabet
;   (n-1) + (n-2) + (n-3) + ... 1 = n * (n - 1) / 2 -> O(n^2)
