(put 'sub '(polynomial polynomial)
     (lambda p1 p2) (tag (sub-poly p1 p2)))

(define (sub-poly p1 p2)
  (add-poly p1 (negate p2)))

(define (negate x) (apply-generic 'negate x))

; Must be added to each respective package:
(put 'negate '(scheme-number)
     (lambda (x) (tag (- x))))
(put 'negate '(rational)
     (lambda (x) (make-rational (- (numer x)) (denom x))))
(put 'negate '(complex)
     (lambda (x) (make-complex-from-real-imag (- (real-part x)) (- (imag-part x)))))
(put 'negate '(polnomial)
     (lambda (x) (make-poly (variable x) (negate-terms (term-list x)))))

(define (negate-terms terms)
  (if (empty-termlist? terms)
      the-empty-termlist
      (let (t (first-term terms))
        (adjoin-term (make-term (order t) (negate (coeff t)))
                     (negate-terms (rest-terms terms)))))
