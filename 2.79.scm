(define (equ? x y) (apply-generic 'equ? x y))

; Must be added to each respective package:
(put 'equ? '(scheme-number scheme-number)
     (lambda (x y) (= x y)))
(put 'equ? '(rational rational)
     (lambda (x y) (and (= (numer x) (numer y)) (= (denom x) (denom y)))))
(put 'equ? '(complex complex)
     (lambda (x y) (and (= (magnitude x) (magnitude y))
                        (= (angle x) (angle y)))))
