(define (make-interval a b) (cons a b))

; Ex. 2.7
(define (lower-bound x) (car x))
(define (upper-bound x) (cdr x))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval x 
                (make-interval (/ 1.0 (upper-bound y))
                               (/ 1.0 (lower-bound y)))))

(define x (make-interval 1.0 2.0))
(define y (make-interval 3.0 4.0))

(add-interval x y)
(mul-interval x y)
(div-interval x y)

; Ex. 2.8
(define (sub-interval x y)
  (make-interval (- (lower-bound x) (upper-bound y))
                 (- (upper-bound x) (lower-bound y))))
(sub-interval x y)

; Ex. 2.9
; Division by 2 omitted for brevity
; ("The width of an interval is half of the difference...").
;
;  wx = x2 - x1
;  wy = y2 - y1
;
;  s = x + y = [x1 + y1, x2 + y2]
;  ws = (x2 + y2) - (x1 + y1) = x2 - x1 + y2 - y1 = wx + wy
;
;  d = x - y = [x1 - y2, x2 - y1]
;  wd = (x2 - y1) - (x1 - y2) = x2 - x1 + y2 - y1 = wx + wy
;
;  x = y = [1, 2] => wx = wy = 1
;  p = x * y = [1, 4] => pw = 3
;  q = x / y = [1, 4] * [0.25, 1] = [0.25, 4] => wq = 3.75
;
;  x = y = [2, 3] => wx = wy = 1
;  p = x * y = [4, 9] => pw = 5
;  q = x / y = [2, 3] * [0.3(3), 0.5] = [0.6(6), 1.5] => wq = 0.83(3)

; Ex. 2.10
(define (div-interval x y)
  (if (< (* (lower-bound y) (upper-bound y)))
    (error "It is not clear what it means to divide by an interval that spans zero."))
  (mul-interval x 
                (make-interval (/ 1.0 (upper-bound y))
                               (/ 1.0 (lower-bound y)))))
(div-interval (make-interval 1 2) (make-interval -1 1))

; Ex. 2.11
;
; - -, - - Bla blah boring
; - -, - +
; - -, + +
; - +, - -
; - +, - +
; - +, + +
; + +, - -
; + +, - +
; + +, + +

; Ex. 2.12
(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))
(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

(define (make-center-percent c p)
  (make-center-width c (/ (* (abs c) p) 100)))
(define (percent i)
  (if (= (center i) 0)
    0
    (* (/ (width i) (abs (center i))) 100)))

(define hundred-ten (make-center-percent 100.0 10.0))
(center hundred-ten)
(width hundred-ten)
(percent hundred-ten)

(define hundred-ten (make-center-percent -100.0 10.0))
(center hundred-ten)
(width hundred-ten)
(percent hundred-ten)

(define hundred-ten (make-center-percent 0.0 10.0))
(center hundred-ten)
(width hundred-ten)
(percent hundred-ten)

; Ex. 2.13
; i1 = [(1 - p1)c1, (1 + p1)c1]
; i2 = [(1 - p2)c2, (1 + p2)c2]
; i1 * i2 = [(1 - p1)(1 - p2)c1c2, (1 + p1)(1 + p2)c1c2]
;           where all endpoints are positive
;   = [(1 - (p1 + p2) + p1p2)c1c2, (1 + (p1 + p2) + p1p2)c1c2]
;   ~= [(1 - (p1 + p2))c1c2, (1 + (p1 + p2)c1c2] for small p1, p2
(define prod1 (mul-interval (make-center-percent 100.0 0.1)
                            (make-center-percent 100.0 0.1)))
(display prod1)
(center prod1)
(percent prod1)
(define prod2 (mul-interval (make-center-percent 100.0 1.0)
                            (make-center-percent 100.0 1.0)))
(display prod2)
(center prod2)
(percent prod2)
(define prod3 (mul-interval (make-center-percent 100.0 10.0)
                            (make-center-percent 100.0 10.0)))
(display prod3)
(center prod3)
(percent prod3)
(define prod4 (mul-interval (make-center-percent 100.0 50.0)
                            (make-center-percent 100.0 50.0)))
(display prod4)
(center prod4)
(percent prod4)
