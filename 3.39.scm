; Q: Which of the five possibilities in the parallel execution shown above remain
; if we instead serialize execution as follows:

(define x 10)

(define s (make-serializer))

(parallel-execute (lambda () (set! x ((s (lambda () (* x x))))))
                  (s (lambda () (set! x (+ x 1)))))


; A:
; 101
; 121
; 11: The original sequence "P2 accesses x, then P1 sets x to 100, then P2 sets
; x" is not possible.  If P2 gets to x first, it will set 11 before P1 computes
; the new value. However, the following sequence -- which also produces 11 --
; is possible: P1 computes the new value as 100, P2 computes the new value as
; 11, P1 sets 100, P2 sets 11. Note that set! from P1 can interleave with P2,
; because it's not serialized.
; 100
