(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
        (else (remainder (* base (expmod base (- exp 1) m)) m))))

(define (expmod2 base exp m)
  (remainder (fast-expt base exp) m))
(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(trace expmod)
(trace expmod2)
(trace fast-expt)

(expmod 123 100003 100003)
(expmod2 123 100003 100003)

; When looking for primes we get to use large numbers as exponents. In the case
; of (expmod2) this results to extremely large intermediate results, making
; computations slower and using up more memory.
