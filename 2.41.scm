(define (enumerate-interval low high)
  (if (> low high)
      '() 
      (cons low (enumerate-interval (+ low 1) high))))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (triples-less-than n)
  (flatmap (lambda (i)
             (flatmap (lambda (j)
                        (map (lambda (k)
                               (list i j k))
                             (enumerate-interval 1 (- j 1))))
                      (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 n)))

(define (triples-that-sum-to s n)
  (filter (lambda (triple) (= (accumulate + 0 triple) s))
          (triples-less-than n)))

(triples-that-sum-to 8 6)
