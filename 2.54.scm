(define (myequal? a b)
  (cond ((and (pair? a) (pair? b))
           (and (myequal? (car a) (car b)) (myequal? (cdr a) (cdr b))))
        (else (eq? a b))))

(define (test x y)
  (eq? (myequal? x y) (equal? x y)))
(test '(this is a list) '(this is a list))
(test '(this is a list) '(this (is a) list))
(test '() '())
(test '() 'a)
(test 'a '())
(test 'a 'b)
(test 'a '(a))
(test 'a 'a)
(test '(1 2 3) '(1 2 3))
