; recursive
(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
                (accumulate combiner null-value term (next a) next b))))

(define (sum term a next b)
  (accumulate + 0 term a next b))

(define (product term a next b)
  (accumulate * 1 term a next b))

(define (identity a) a)

(define (inc a) (+ a 1))

(sum cube 1 inc 10)
(product identity 1 inc 4)

; iteratitve
; NOTE: `sum` and `product` will use the re-defined `accumulate` automatically,
;       which can be checked easily, e.g., by returning (+ result 12) instead
;       of result.
(define (accumulate combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner (term a) result))))
  (iter a  null-value))

(sum cube 1 inc 10)
(product identity 1 inc 4)
