(define (pick7 tree)
  (car (cdr (car (cdr (cdr tree))))))

(pick7 (list 1 3 (list 5 7) 9))

(define (pick7 tree)
  (car (car tree)))

(pick7 (list (list 7)))

(define (pick7 tree)
  (car (cdr (car (cdr (car (cdr (car (cdr (car (cdr (car (cdr tree)))))))))))))

(pick7 (list 1 (list 2 (list 3 (list 4 (list 5 (list 6 7)))))))

; Using `cadr` as a shortcut:
(define (pick7 tree)
  (cadr (cadr (cadr (cadr (cadr (cadr tree)))))))

(pick7 (list 1 (list 2 (list 3 (list 4 (list 5 (list 6 7)))))))
