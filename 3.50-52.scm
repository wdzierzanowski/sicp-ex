; When testing with Chicken Scheme, we can see that the delayed evaluation is
; "delayed more" than one would expect from reading SICP. For example, in 3.52,
; the first time `accum` is invoked is within the call to `stream-ref`.

(import streams)

(define (display-line . x)
  (display x)
  (newline))

(define (display-stream s)
  (stream-for-each display-line s))

(define (stream-enumerate-interval low high)
  (if (> low high)
      stream-null
      (stream-cons
       low
       (stream-enumerate-interval (+ low 1) high))))

; 3.50
(define (stream-map proc . argstreams)
  (if (stream-null? (car argstreams))
      stream-null
      (stream-cons
       (apply proc (map stream-car argstreams))
       (apply stream-map
              (cons proc (map stream-cdr argstreams))))))

; 3.51
(define (show x)
  (display-line x)
  x)

(define x (stream-map show (stream-enumerate-interval 0 10)))
(stream-ref x 5)
(stream-ref x 7)

; 3.52
; What is the value of sum after each of the above expressions is evaluated?
(define sum 0)
(define (accum x)
  (set! sum (+ x sum))
  (display-line 'sum: sum)
  sum)
(display-line 'define-seq)
(define seq (stream-map accum (stream-enumerate-interval 1 20)))
(display-line 'define-y)
(define y (stream-filter even? seq))
(display-line 'define-z)
(define z (stream-filter (lambda (x) (= (remainder x 5) 0))
                         seq))
(display-line 'stream-ref-y-7)
(stream-ref y 7)
(display-line 'display-stream-z)
(display-stream z)
