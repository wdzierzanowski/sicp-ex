; a. Each file must install a `get-record` procedure under the division name
; tag (as returned by `division`). We annotate the return value with the
; division name to be able to do b.
(define (get-record name file)
  (let ((division-name (division file)))
    (let ((record ((get 'get-record (list division-name)) name)))
      (if record
          (attach-tag division-name record)
          #f))))

; b. Each file must install a `get-salary procedure under the division name
; tag.
(define (get-salary tagged-record)
  (let ((division-name (type-tag tagged-record)))
    ((get 'get-salary (list division-name)) (contents record))))

; c.
(define (find-employee-record name records)
  (if (null? records)
      #f
      (let ((record (get-record name (car records))))
        (if record
            record
            (find-employee-record name (cdr records))))))

; d. Make sure the new company's file installs `get-record` and `get-salary`.
