(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (list->tree elements)
  (car (partial-tree elements (length elements))))

(define (partial-tree elts n)
  (if (= n 0)
      (cons '() elts)
      (let ((left-size (quotient (- n 1) 2)))
        (let ((left-result (partial-tree elts left-size)))
          (let ((left-tree (car left-result))
                (non-left-elts (cdr left-result))
                (right-size (- n (+ left-size 1))))
            (let ((this-entry (car non-left-elts))
                  (right-result (partial-tree (cdr non-left-elts)
                                              right-size)))
              (let ((right-tree (car right-result))
                    (remaining-elts (cdr right-result)))
                (cons (make-tree this-entry left-tree right-tree)
                      remaining-elts))))))))

; How partial-tree works:
;  - The size of the left subtree is determined. The result is balanced, hence
;    the division n /2.
;  - Recursively call partial-tree to get the
;    {left subtree, elements not in left subtree} pair. The left subtree
;    contains elements smaller than the median.
;  - Recursively call partial-tree on the elements not in the left subtree
;    (exluding the first one) to get the {right subtree, remaining elements}
;    pair.
;  - The entry for the current node is the first of the elements that didn't
;    make it into the left subtree (the median), the left branch is the left
;    (smaller elements) subtree, the right branch is the right (greater
;    elements) subtree.
;
; What does (list->tree '(1 3 5 7 9 11)) produce?
;
;         5
;    1        9
;      3    7  11

(list->tree '(1 3 5 7 9 11))
