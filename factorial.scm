(define (factorial-recursive n)
  (if (= n 1)
    1
    (* n (factorial-recursive (- n 1)))))
(trace factorial-recursive)

(define (factorial-iterative n)
  (define (iter product counter max-count)
    (if (> counter max-count)
      product
      (iter (* counter product) (+ counter 1) max-count)))
  (trace iter)
  (iter 1 1 n))

(factorial-recursive 5)
(factorial-iterative 5)
