; deque

(define (make-deque)
  (let ((node (make-node '())))
    (set-prev-node! node node)
    (set-next-node! node node)
    node))

(define (empty-deque? deque) (eq? (next-node deque) deque))

(define (front-deque deque)
  (if (empty-deque? deque)
      (error "FRONT called with an empty deque" deque)
      (value-node (next-node deque))))

(define (rear-deque deque)
  (if (empty-deque? deque)
      (error "REAR called with an empty deque" deque)
      (value-node (prev-node deque))))

(define (front-insert-deque! deque item)
  (let ((new-node (make-node item)))
    (set-prev-node! new-node deque)
    (set-next-node! new-node (next-node deque))
    (set-prev-node! (next-node deque) new-node)
    (set-next-node! deque new-node)
    deque))

(define (rear-insert-deque! deque item)
  (let ((new-node (make-node item)))
    (set-prev-node! new-node (prev-node deque))
    (set-next-node! new-node deque)
    (set-next-node! (prev-node deque) new-node)
    (set-prev-node! deque new-node)
    deque))

(define (front-delete-deque! deque)
  (cond ((empty-deque? deque)
         (error "FRONT DELETE! called with an empty deque" deque))
        (else
          (let ((second-node (next-node (next-node deque))))
            (set-next-node! deque second-node)
            (set-prev-node! second-node deque)
            deque))))

(define (rear-delete-deque! deque)
  (cond ((empty-deque? deque)
         (error "REAR DELETE! called with an empty deque" deque))
        (else
          (let ((second-to-last-node (prev-node (prev-node deque))))
            (set-prev-node! deque second-to-last-node)
            (set-next-node! second-to-last-node deque)
            deque))))

(define (print-deque deque)
  (define (iter node)
    (if (not (null? (value-node node)))
      (begin (display (value-node node))
             (display " ")
             (iter (next-node node)))))
  (display "( ")
  (iter (next-node deque))
  (display ")")
  (newline))

; node (in a doubly-linked list):
;   A pair of `value` and `a pair of prev, next`.

(define (make-node value) (cons value (cons '() '())))
(define (value-node node) (car node))
(define (next-node node) (cddr node))
(define (prev-node node) (cadr node))
(define (set-next-node! node next-node) (set-cdr! (cdr node) next-node))
(define (set-prev-node! node prev-node) (set-car! (cdr node) prev-node))

(define dq (make-deque))
(print-deque (front-insert-deque! dq 'c))
(print-deque (front-insert-deque! dq 'b))
(print-deque (rear-insert-deque! dq 'd))
(print-deque (front-insert-deque! dq 'a))
(print-deque (rear-delete-deque! dq))
(print-deque (front-delete-deque! dq))
(print-deque (rear-insert-deque! dq 'e))
(print-deque (front-delete-deque! dq))
(print-deque (rear-delete-deque! dq))
(print-deque (rear-delete-deque! dq))
