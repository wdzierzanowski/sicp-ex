(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

; In-order search, so
;   { 1, 3, 5, 7, 9, 11 }
;   { 1, 3, 5, 7, 9, 11 }
;   { 1, 3, 5, 7, 9, 11 }
; O(n), have to visit each entry exactly once. (?)
; (But probably worse than that, depending on how append is implemented.)
(define (tree->list-1 tree)
  (if (null? tree)
      '()
      (append (tree->list-1 (left-branch tree))
              (cons (entry tree)
                    (tree->list-1 (right-branch tree))))))

;   { 1, 3, 5, 7, 9, 11 }
;   { 1, 3, 5, 7, 9, 11 }
;   { 1, 3, 5, 7, 9, 11 }
; O(n), have to visit each entry exactly once.
(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
        result-list
        (copy-to-list (left-branch tree)
                      (cons (entry tree)
                            (copy-to-list (right-branch tree)
                                          result-list)))))
  (copy-to-list tree '()))

(define t1 (make-tree 7 (make-tree 3 (make-tree 1 '() '()) (make-tree 5 '() '()))
                        (make-tree 9 '() (make-tree 11 '() '()))))

(define t2 (make-tree 3 (make-tree 1 '() '())
                        (make-tree 7 (make-tree 5 '() '())
                                     (make-tree 9 '() (make-tree 11 '() '())))))

(define t3 (make-tree 5 (make-tree 3 (make-tree 1 '() '()) '())
                        (make-tree 9 (make-tree 7 '() '()) (make-tree 11 '() '()))))

(tree->list-1 t1)
(tree->list-2 t1)
(tree->list-1 t2)
(tree->list-2 t2)
(tree->list-1 t3)
(tree->list-2 t3)
