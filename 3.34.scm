; Louis Reasoner wants to build a squarer, a constraint device with two
; terminals such that the value of connector b on the second terminal will
; always be the square of the value a on the first terminal. He proposes the
; following simple device made from a multiplier:

(define (squarer a b)
  (multiplier a a b))

; Q: There is a serious flaw in this idea. Explain.

; A: The squarer would work properly in the a^2 = b direction, but it would
; fail in the opposite direction: If you set a value on `b`, neither of the
; multiplier's `m1`, `m2` connnectors has a value, so it will do nothing.
