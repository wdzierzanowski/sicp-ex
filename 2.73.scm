(define (deriv expr var)
   (cond ((number? expr) 0)
         ((variable? expr) (if (same-variable? expr var) 1 0))
         (else ((get 'deriv (operator expr)) (operands expr)
                                            var))))
(define (operator expr) (car expr))
(define (operands expr) (cdr expr))

; a. Explain what was done above.
;   - We use a table to look up the derivation operations by operator and thus
;     perform data-directed dispatch.
;
; Why can't we assimilate the predicates `number?` and `same-variable?` into
; the data-directed dispatch?
;   - We key into the table by operator type, and a number/variable has no
;     operator.

; b. c.
(define (variable? x) (symbol? x))
(define (same-variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))

(define (install-derivatives)
  ; Helpers
  (define (make-sum a1 a2) (list '+ a1 a2))
  (define (make-product m1 m2) (list '* m1 m2))
  (define (make-exponentation base exponent)
    (list '** base exponent))
  (define (addend s) (cadr s))
  (define (augend s) (caddr s))
  (define (multiplier p) (cadr p))
  (define (multiplicand p) (caddr p))
  (define (base e) (cadr e))
  (define (exponent e) (caddr e))
  ; Derivation procedures
  (define (deriv-sum expr var)
    (make-sum (deriv (addend expr) var)
              (deriv (augend expr) var)))
  (define (deriv-product expr var)
     (make-sum
       (make-product (multiplier expr)
                     (deriv (multiplicand expr) var))
       (make-product (deriv (multiplier expr) var)
                     (multiplicand expr))))
  (define (deriv-exponentation expr var)
       (make-product
         (make-product (exponent exp)
                       (make-exponentation (base exp) (- (exponent exp) 1)))
         (deriv (base exp) var)))
  ; Install derivation procedures
  (put 'deriv '(+) deriv-sum)
  (put 'deriv '(*) deriv-product)
  (put 'deriv '(**) deriv-exponentation))

; d.
; Switch the order when calling `put` in `install-derivatives`, and when
; calling `get` in `deriv`.
