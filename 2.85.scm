(define (equ? x y) (apply-generic 'equ? x y))

(put 'equ? '(integer integer)
     (lambda (x y) (= (content x) (content y))))
(put 'equ? '(rational rational)
     (lambda (x y) (and (= (numer x) (numer y)) (= (denom x) (denom y)))))
(put 'equ? '(real real)
     (lambda (x y) (= (content x) (content y))))
(put 'equ? '(complex complex)
     (lambda (x y) (and (= (magnitude x) (magnitude y))
                        (= (angle x) (angle y)))))

(define (project x) (apply-generic 'project x))

(put 'project '(integer) (lambda (x) #f))
(put 'project '(rational)
     (lambda (x) (make-integer (round (/ (numer x) (denom x))))))
(put 'project '(real) (lambda (x) (make-rational (rationalize x .001) 1)))
(put 'project '(complex) (lambda (x) (make-real (magnitude x))))

(define (drop x)
  (let ((lowered-x (project x)))
    (cond ((not lowered-x) x)
          ((equ? x (raise lowered-x)) (drop lowered-x))
          (else x))))

; Now `apply-generic` must simply invoke `drop` before returning:
          (drop (apply proc (map contents args)))
