; Charmichael numbers are numbers that fool the Fermat test. For each such
; number n, a^n can be congruent to a modulo n even though n isn't prime.
; 
; The smallest Charmichael numbers:
; 561, 1105, 1729, 2465, 2821, 6601

(define (charmichael-test n)
  (newline)
  (do-charmichael-test n 1))

(define (do-charmichael-test n a)
  (cond ((= n a) (report-success n))
        ((= (expmod a n n) a) (do-charmichael-test n (+ a 1)))
        (else (report-failure n a))))

(define (report-success n)
  (display n)
  (if (prime? n)
      (display " is prime")
      (display ": Charmichael approves")))

(define (report-failure n a)
  (display "Checking ")
  (display n)
  (display " against ")
  (display a)
  (display ": Fermat test failed => not prime"))

(define (prime? n)
  (= n (smallest-divisor n)))
(define (smallest-divisor n)
  (find-divisor n 2))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))
(define (divides? a b)
  (= (remainder b a) 0))
(define (next a)
  (if (= a 2)
      3
      (+ a 2)))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp) (remainder (square (expmod base (/ exp 2) m)) m))
        (else (remainder (* base (expmod base (- exp 1) m)) m))))

(charmichael-test 7)
(charmichael-test 8)
(charmichael-test 11)
(charmichael-test 561)
(charmichael-test 1105)
(charmichael-test 1729)
(charmichael-test 2465)
(charmichael-test 2821)
(charmichael-test 6601)
