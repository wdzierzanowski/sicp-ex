(define (my-exp b n)
  (my-exp-iter 1 b n))

(define (my-exp-iter a b n)
  (cond ((= n 0) a)
        ((is_even n) (my-exp-iter a (square b) (/ n 2)))
        (else (my-exp-iter (* a b) (square b) (/ (- n 1) 2)))))

(define (is_even x)
  (= (remainder x 2) 0))

(my-exp 2 5)
(my-exp 2 13)
(my-exp 3 3)
(my-exp 8 7)
