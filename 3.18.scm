(define (last-pair x)
  (if (null? (cdr x))
      x
      (last-pair (cdr x))))

(define (make-cycle x)
  (set-cdr! (last-pair x) x)
  x)

(define z (make-cycle (list 'a 'b 'c)))

(define (contains-cycle? x)
  (let ((seen-items '()))
    (define (iter x)
      (if (not (pair? x))
          #f
          (if (memq (car x) seen-items)
              #t
              (begin (set! seen-items (cons (car x) seen-items))
                     (iter (cdr x))))))
    (iter x)))

(print (contains-cycle? (list 'a 'b 'c)))
(print (contains-cycle? z))
