(define l (list 1 4 9 16 25))

(define (reverse sequence)
  (fold-right (lambda (item prev-items) (append prev-items (list item)))
              '()
              sequence))

(reverse l)

(define (reverse sequence)
  (fold-left (lambda (prev-items item) (cons item prev-items))
             '()
             sequence))

(reverse l)
