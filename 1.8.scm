(define (good-enough? prev-guess guess)
  (< (/ (abs (- prev-guess guess)) guess) 0.000001))

(define (square x)
  (* x x))

(define (improve guess x)
  (/ (+ (/ x (square guess)) (* 2 guess)) 3))

(define (curt-iter prev-guess guess x)
  (if (good-enough? prev-guess guess)
      guess
      (curt-iter guess (improve guess x) x)))

(define (curt x)
  (curt-iter 0 1 x))

(curt 8.0)
(curt 27.0)
(curt 0.000001)
(curt 1000000.0)
(curt 10000000000.0)
