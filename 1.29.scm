(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (inc n)
  (+ n 1))
(define (sum-cubes a b)
  (sum cube a inc b))

(sum-cubes 1 10)

(define (integral f a b dx)
  (define (add-dx x) (+ x dx))
  (* (sum f (+ a (/ dx 2)) add-dx b)
     dx))

(integral cube 0 1 0.01)
(integral cube 0 1 0.001)

; y_0 + 4y_1 + 2y_2 + 4y_3 + 2y_4 + ... + 2y_(n-2) + 4y_(n-1) + y_n =
; y_0 + y_n + 4(y_1 + y_3 + ... + y_(n-1)) + 2(y_2 + y_4 + ... + y_(n-2))
;
; y_k = f(a + kh)
(define (simpson-integral f a b n)
  (define h (/ (- b a) n))
  (define (add-2h x) (+ x h h))
  (* (+ (f a)
        (f b)
        (* 4 (sum f (+ a h) add-2h b))
        (* 2 (sum f (+ a h h) add-2h b)))
     (/ h 3)))

(simpson-integral cube 0.0 1.0 100)
(simpson-integral cube 0.0 1.0 1000)
