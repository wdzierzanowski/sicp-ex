(define (or-gate a1 a2 output)
  (let ((na1 (make-wire))
        (na2 (make-wire))
        (na1-and-na2 (make-wire)))
    (inverter a1 na1)
    (inverter a2 na2)
    (and-gate na1 na2 na1-and-na2)
    (inverter na1-and-na2 output)
    'ok))

; Delay time: 2 * iverter-delay + and-delay
