(define (print n)
  ((n (lambda (x) (display "."))) "ignored"))

(define zero (lambda (f) (lambda (x) x)))

(define (add-1 n)
  (lambda (f) (lambda (x) (f ((n f) x)))))

; (add-1 zero)
; -> (lambda (f) (lambda (x) (f ((lambda (x) x) x))))
; -> (lambda (f) (lamdda (x) (f x)))
(define one (lambda (f) (lambda (x) (f x))))

; (add-1 one)
; -> (lambda (f) (lambda (x) (f ((lambda (x) (f x)) x))))
; -> (lambda (f) (lambda (x) (f (f x)))
(define two (lambda (f) (lambda (x) (f (f x)))))

(define (+ a b)
  (lambda (f) (lambda (x) ((b f) ((a f) x)))))

(print one)
(print two)

(define three (+ one two))
(print three)

(define five (+ two three))
(print five)

(print (+ five five))
