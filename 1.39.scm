(define (cont-frac n d k)
  (define (frac i)
    (if (> i k)
        0
        (/ (n i) (+ (d i) (frac (+ i 1))))))
  (trace frac)
  (frac 1))

(define (tan-cf x k)
  (cont-frac (lambda (i) (if (= i 1)
                             x
                             (- 0.0 (square x))))
             (lambda (i) (- (* i 2) 1))
             k))

(tan-cf (/ 3.14159265 6) 18)
(tan (/ 3.14159265 6))
