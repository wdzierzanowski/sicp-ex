(define (=zero? x) (apply-generic '=zero? x))

; Must be added to each respective package:
(put '=zero? '(scheme-number)
     (lambda (x) (= x 0)))
(put '=zero? '(rational)
     (lambda (x) (= (numer x) 0)))
(put '=zero? '(complex)
     (lambda (x) (= (magnitude x) 0)))
