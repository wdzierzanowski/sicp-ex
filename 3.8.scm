(define f
  (let ((prev 0))
    (lambda (x)
      (let ((ret prev))
        (set! prev x)
        ret))))
