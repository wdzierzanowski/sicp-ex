;1 ]=> (define x (list 1 2 3))
;1 ]=> (define y (list 4 5 6))

;1 ]=> (append x y) 
;Value: (1 2 3 4 5 6)

; ]=> (cons x y)
;Value: ((1 2 3) 4 5 6)
; Suprised? Note that (list 4 5 6) is equivalent to,
; (cons 4 (cons 5 (cons 6 nil))). Thus, (cons x y) addsthe _list_ (1 2 3) --
; and not its elements separately -- as the first elment to the list (4 5 6).

; 1 ]=> (list x y)
;Value: ((1 2 3) (4 5 6))
