(define (last-pair l)
  (let ((p (cdr l))
    if (null? (cdr p))
       p
       (last-pair (cdr l)))))

(last-pair (list 23 72 149 34))
