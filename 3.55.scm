(import streams)

(define (add-streams s1 s2)
  (stream-map + s1 s2))

(define (partial-sums S)
  (stream-cons
    (stream-car S)
    (add-streams (partial-sums S) (stream-cdr S))))

; http://community.schemewiki.org/?sicp-ex-3.55
(define (partial-sums s) 
   (add-streams s (stream-cons 0 (partial-sums s)))) 

(define ones (stream-cons 1 ones))
(define integers (stream-cons 1 (add-streams ones integers)))
(define x (partial-sums integers))

(print (stream-ref x 0))
(print (stream-ref x 1))
(print (stream-ref x 2))
(print (stream-ref x 3))
(print (stream-ref x 4))
(print (stream-ref x 5))
