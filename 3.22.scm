(define (make-queue)
  (let ((front-ptr '())
        (rear-ptr '()))
    (define (empty-queue?) (null? front-ptr))
    (define (front-queue) (car front-ptr))
    (define (insert-queue! item)
      (let ((tail (list item)))
        (if (null? rear-ptr)
            (begin (set! front-ptr tail)
                   (set! rear-ptr tail))
            (begin (set-cdr! rear-ptr tail)
                   (set! rear-ptr tail)))))
    (define (delete-queue!)
      (if (pair? front-ptr)
          (set! front-ptr (cdr front-ptr))))
    (define (print-queue)
      (display "( ")
      (define (iter ptr)
        (if (pair? ptr)
            (begin (display (car ptr))
                   (display " ")
                   (iter (cdr ptr)))))
      (iter front-ptr)
      (display ")")
      (newline))
    (define (dispatch m)
      (cond ((eq? m 'empty-queue?) empty-queue?)
            ((eq? m 'front-queue) front-queue)
            ((eq? m 'insert-queue!) (lambda (item) (insert-queue! item)))
            ((eq? m 'delete-queue!) (delete-queue!))
            ((eq? m 'print-queue) (print-queue))
            (else (error "Undefined operation" m))))
    dispatch))

(define (empty-queue? q) (q 'empty-queue?))
(define (front-queue q) (q 'front-queue))
(define (insert-queue! q item)
  ((q 'insert-queue!) item)
  q)
(define (delete-queue! q)
  (q 'delete-queue!)
  q)
(define (print-queue q) (q 'print-queue))

(define q2 (make-queue))
(print-queue (insert-queue! q2 'a))
(print-queue (insert-queue! q2 'b))
(print-queue (delete-queue! q2))
(print-queue (delete-queue! q2))
