(define (count-pairs x)
  (let ((seen-pairs '()))
    (define (iter x)
      (if (not (pair? x))
          0
          (+ (iter (car x))
             (iter (cdr x))
             (if (memq x seen-pairs)
                 0
                 (begin (set! seen-pairs (cons x seen-pairs))
                        1)))))
    (iter x)))

(print (count-pairs '(a b c)))

(define pair (cons 'a 'b))
(print (count-pairs (list pair pair)))

(define l '(a))
(define pair-of-l (cons l l))
(print (count-pairs (cons pair-of-l pair-of-l)))

(define (last-pair x)
  (if (null? (cdr x))
      x
      (last-pair (cdr x))))

(define loop '(a b c))
(set-cdr! (last-pair loop) loop)
; Never returns:
; (print (count-pairs loop))
