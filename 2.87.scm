(define (=zero? x) (apply-generic '=zero? x))

; Just need to check if the term list is empty because we've assumed we only
; include non-zero terms in the term list.
(put '=zero? '(polynomial)
     (lambda (x) (empty-termlist? (term-list x))))
