(import streams)

(define (stream-add s1 s2)
  (stream-map + s1 s2))
(define ones (stream-cons 1 ones))
(define integers (stream-cons 1 (stream-add ones integers)))

(define (stream-scale stream factor)
  (stream-map (lambda (x) (* x factor)) stream))

; 3.59 a.
(define (integrate-series A)
  (stream-map / A integers))

; 3.59 b.
(define cosine-series
  (stream-cons 1 (stream-scale (integrate-series sine-series) -1)))
(define sine-series
  (stream-cons 0 (integrate-series cosine-series)))

(print 'cosine:)
(print (stream-ref cosine-series 0))
(print (stream-ref cosine-series 1))
(print (stream-ref cosine-series 2))
(print (stream-ref cosine-series 3))
(print (stream-ref cosine-series 4))
(print (stream-ref cosine-series 5))
(print (stream-ref cosine-series 6))
(print (stream-ref cosine-series 7))
(print 'sine:)
(print (stream-ref sine-series 0))
(print (stream-ref sine-series 1))
(print (stream-ref sine-series 2))
(print (stream-ref sine-series 3))
(print (stream-ref sine-series 4))
(print (stream-ref sine-series 5))
(print (stream-ref sine-series 6))
(print (stream-ref sine-series 7))

; 3.60, http://community.schemewiki.org/?sicp-ex-3.60
(define (mul-series s1 s2)
  (stream-cons (* (stream-car s1) (stream-car s2))
               (stream-add (stream-scale (stream-cdr s2) (stream-car s1))
                           (mul-series (stream-cdr s1) s2))))

(define one (stream-add (mul-series sine-series sine-series)
                        (mul-series cosine-series cosine-series)))

(print 'one:)
(print (stream-ref one 0))
(print (stream-ref one 1))
(print (stream-ref one 2))
(print (stream-ref one 3))
(print (stream-ref one 4))
(print (stream-ref one 5))
(print (stream-ref one 6))
