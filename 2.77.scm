(define (magnitude z) (apply-generic 'magnitude z))

(put 'magnitude '(complex) magnitude)

(magnitude (make-complex-from-real-imag 3 4))
; First, `apply-generic` uses the name of the operation "magnitude" and the
; type tag "complex" to get the same `magnitude` procedure while stripping
; the argument of the "complex" type tag. Invoking the procedure now means
; invoking `apply-generic` again to look up by the same operation name
; "magnitude" but the type tag is now "rectangular" and thus we arrive at the
; procedure defined in the "rectangular" package internally.
