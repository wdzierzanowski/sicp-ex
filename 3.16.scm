(define (count-pairs x)
  (if (not (pair? x))
      0
      (+ (count-pairs (car x))
         (count-pairs (cdr x))
         1)))

(print (count-pairs '(a b c)))

(define pair (cons 'a 'b))
(print (count-pairs (list pair pair)))

(define l '(a))
(define pair-of-l (cons l l))
(print (count-pairs (cons pair-of-l pair-of-l)))

(define (last-pair x)
  (if (null? (cdr x))
      x
      (last-pair (cdr x))))

(define loop '(a b c))
(set-cdr! (last-pair loop) loop)
; Never returns:
; (print (count-pairs loop))
