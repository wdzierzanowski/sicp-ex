(define (good-enough? prev-guess guess)
  (< (/ (abs (- prev-guess guess)) guess) 0.000001))

(define (improve guess x)
  (/ (+ guess (/ x guess)) 2))

(define (sqrt-iter prev-guess guess x)
  (if (good-enough? prev-guess guess)
      guess
      (sqrt-iter guess (improve guess x) x)))

(define (sqrt x)
  (sqrt-iter 0 1 x))

(sqrt 4.0)
(sqrt 0.0004)
(sqrt 1000000.0)
(sqrt 10000000000.0)
