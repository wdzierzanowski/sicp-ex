(import streams)

(define (merge s1 s2)
  (cond ((stream-null? s1) s2)
        ((stream-null? s2) s1)
        (else
         (let ((s1car (stream-car s1))
               (s2car (stream-car s2)))
           (cond ((< s1car s2car)
                  (stream-cons s1car (merge (stream-cdr s1) s2)))
                 ((> s1car s2car)
                  (stream-cons s2car (merge s1 (stream-cdr s2))))
                 (else
                  (stream-cons s1car
                               (merge (stream-cdr s1)
                                      (stream-cdr s2)))))))))

(define (stream-scale stream factor)
  (stream-map (lambda (x) (* x factor)) stream))

(define S (stream-cons 1 (merge (merge (stream-scale S 3)
                                       (stream-scale S 5))
                                (stream-scale S 2))))

(print (stream-ref S 0))
(print (stream-ref S 1))
(print (stream-ref S 2))
(print (stream-ref S 3))
(print (stream-ref S 4))
(print (stream-ref S 5))
(print (stream-ref S 6))
(print (stream-ref S 7))
(print (stream-ref S 8))
(print (stream-ref S 9))
(print (stream-ref S 10))
(print (stream-ref S 11))
(print (stream-ref S 12))
(print (stream-ref S 13))
