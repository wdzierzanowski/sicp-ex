; 1 + 1/Φ = 1 + 2/(1 + √5) = (1 + √5 + 2) / (1 + √5) =
;   (3 + √5) / (1 + √5) = (2 * (3 + √5)) / (2 * (1 + √5) =
;   (6 + 2√5) / (2 * (1 + √5)) = (1 + 2√5 + 5) / (2 * (1 + √5)) =
;   (1 + √5)^2 / (2 * (1 + √5)) = (1 + √5) / 2 = Φ

(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.5)
