(define s (cons-stream 1 (add-streams s s)))
; `s` is a stream whose first element is 1, and the second element is the sum
; of the first element and itself, or 2. The third element is the sum of the
; second element and itself, or 4, etc., i.e.: 1, 2, 4, 8, 16, 32 (powers of
; 2).
