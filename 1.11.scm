(define (calc f-1 f-2 f-3)
  (+ f-1 (* 2 f-2) (* 3 f-3)))

; Recursive process

(define (fr n)
  (if (< n 3)
      n
      (calc (fr (- n 1)) (fr (- n 2)) (fr (- n 3)))))

; Iterative process

(define (fi n)
  (if (< n 3)
      n
      (f-iter 3 n 2 1 0)))

(define (f-iter i n f-1 f-2 f-3)
  (if (< i n)
      (f-iter (+ i 1) n (calc f-1 f-2 f-3) f-1 f-2)
      (calc f-1 f-2 f-3)))

; Iterative process with lexical scoping of n

(define (fils n)
  (define (f-iter i f-1 f-2 f-3)
    (if (< i n)
      (f-iter (+ i 1) (calc f-1 f-2 f-3) f-1 f-2)
      (calc f-1 f-2 f-3)))
  (if (< n 3)
      n
      (f-iter 3 2 1 0)))

(fr 0)
(fr 1)
(fr 2)
(fr 3)
(fr 4)
(fr 5)
(fr 6)

(fi 0)
(fi 1)
(fi 2)
(fi 3)
(fi 4)
(fi 5)
(fi 6)

(fils 0)
(fils 1)
(fils 2)
(fils 3)
(fils 4)
(fils 5)
(fils 6)
