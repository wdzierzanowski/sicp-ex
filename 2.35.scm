(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (enumerate-tree tree)
  (cond ((null? tree) '())
        ((not (pair? tree)) (list tree))
        (else (append (enumerate-tree (car tree))
                      (enumerate-tree (cdr tree))))))

(define x (cons (list 1 2) (list 3 4)))
(define xx (list x x))

(define (count-leaves x)
  (cond ((null? x) 0)  
        ((not (pair? x)) 1)
        (else (+ (count-leaves (car x))
                 (count-leaves (cdr x))))))

(count-leaves x)
(count-leaves xx)

(define (count-leaves t)
  (accumulate (lambda (next sum) (+ sum 1))
              0
              (map (lambda (x) x) (enumerate-tree t))))

(count-leaves x)
(count-leaves xx)

; Simpler:
(define (count-leaves t)
  (accumulate +
              0
              (map (lambda (x) 1) (enumerate-tree t))))

(count-leaves x)
(count-leaves xx)

; Without enumerate-tree:
(define (count-leaves t)
  (accumulate +
              0
              (map (lambda (x) (if (pair? x)
                               (count-leaves x)
                               1))
                   t)))

(count-leaves x)
(count-leaves xx)
