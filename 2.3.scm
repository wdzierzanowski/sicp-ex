; Note: The exercise text just says "rectangles in a plane", but I've narrowed
; it down to rectangles with sides parallel / perpendicular to the axes for
; simpliity. This does not take away from the goal of the exercise in any way.

(define (print-rect r)
  (display "(")
  (display (left-rect r))
  (display ",")
  (display (top-rect r))
  (display ",")
  (display (right-rect r))
  (display ",")
  (display (bottom-rect r))
  (display "("))

(define (make-segment p q)
  (cons p q))

(define (start-segment s)
  (car s))

(define (end-segment s)
  (cdr s))

(define (length-segment s)
  (let ((a (- (y-point (end-segment s)) (y-point (start-segment s))))
        (b (- (x-point (end-segment s)) (x-point (start-segment s)))))
    (sqrt (+ (square a) (square b)))))

(define (make-point x y)
  (cons x y))

(define (x-point p)
  (car p))

(define (y-point p)
  (cdr p))

(define (perim-rect r)
  (+ (* (height-rect r) 2)
     (* (width-rect r) 2)))

(define (area-rect r)
  (* (height-rect r) (width-rect r)))

(define (test r)
  (newline)
  (display "Rectangle ")
  (display r)
  (display " has perimeter ")
  (display (perim-rect r))
  (display " and area ")
  (display (area-rect r)))

; Representation: a pair of the left and top segments.
(define (make-rect left top right bottom)
  (let ((left-segment (make-segment (make-point left top) (make-point left bottom)))
        (top-segment (make-segment (make-point left top) (make-point right top))))
    (cons left-segment top-segment)))

(define (width-rect r)
  (length-segment (cdr r)))

(define (height-rect r)
  (length-segment (car r)))

(test (make-rect 0 1 1 0))
(test (make-rect -2 2 2 -2))

; New representation: a pair of pairs ((left, right), (top, bottom))
(define (make-rect left top right bottom)
  (cons (cons left right) (cons top bottom)))

(define (width-rect r)
  (abs (- (cdr (car r)) (car (car r)))))

(define (height-rect r)
  (abs (- (cdr (cdr r)) (car (cdr r)))))

(test (make-rect 0 1 1 0))
(test (make-rect -2 2 2 -2))
