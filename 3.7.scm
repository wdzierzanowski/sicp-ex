(define (make-account balance password)
  (define (good-password? p)
    (eq? p password))
  (define (withdraw amount)
    (if (>= balance amount)
        (begin (set! balance (- balance amount))
               balance)
        "Insufficient funds"))
  (define (deposit amount)
    (set! balance (+ balance amount))
    balance)
  (define (dispatch p m)
    (if (good-password? p)
        (cond ((eq? m 'withdraw) withdraw)
              ((eq? m 'deposit) deposit)
              (else (error "Unknown request -- MAKE-ACCOUNT" m)))
        (lambda (swallow-amount) "Incorrect password")))
  dispatch)

(define (make-joint account password new-password)
  (define (good-password? p)
    (eq? p new-password))
  (define (dispatch p m)
    (if (good-password? p)
        (account password m)
        (lambda (swallow-amount) "Incorrect password")))
  dispatch)

(define peter-acc (make-account 100 'open-sesame))

(print ((peter-acc 'open-sesame 'withdraw) 40))
(print ((peter-acc 'some-other-password 'deposit) 50))

(define paul-acc
  (make-joint peter-acc 'open-sesame 'rosebud))

; Good passwords
(print ((peter-acc 'open-sesame 'withdraw) 10))
(print ((paul-acc 'rosebud 'withdraw) 10))
(print ((peter-acc 'open-sesame 'withdraw) 10))
(print ((paul-acc 'rosebud 'deposit) 10))
(print ((peter-acc 'open-sesame 'deposit) 10))

; Bad passwords
(print ((paul-acc 'open-sesame 'deposit) 10))
(print ((peter-acc 'rosebud 'deposit) 10))
(print ((paul-acc 'some-other-password 'deposit) 50))
(print ((peter-acc 'some-other-password 'deposit) 50))
