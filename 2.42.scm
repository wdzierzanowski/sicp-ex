(define (queens board-size)
  (define (queen-cols k)  
    (if (= k 0)
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

(define (enumerate-interval low high)
  (if (> low high)
      '() 
      (cons low (enumerate-interval (+ low 1) high))))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (adjoin-position row col rest-of-queens)
  (append rest-of-queens (list (cons col row))))

(define empty-board '())

(define (safe? k positions)
  (if (null? positions)
      #t
      (let ((k-pos (select-k-pos k positions))
            (other-pos (select-other-pos k positions)))
        (= (accumulate (lambda (pos count-so-far)
                         (+ (if (in-check? k-pos pos) 1 0) count-so-far))
                       0
                       other-pos)
           0))))

(define (select-k-pos k positions)
  (let ((pos (car positions)))
    (if (= k (pos-col pos))
        pos
        (select-k-pos k (cdr positions)))))

(define (select-other-pos k positions)
  (filter (lambda (pos) (not (= k (pos-col pos)))) positions))

(define (in-check? p q)
  (or (= (pos-col p) (pos-col q))
      (= (pos-row p) (pos-row q))
      (= (abs (- (pos-col p) (pos-col q))) (abs (- (pos-row p) (pos-row q))))))

(define (pos-col pos) (car pos))
(define (pos-row pos) (cdr pos))

(queens 5)
