(import (chicken random))

(define (monte-carlo trials experiment)
  (define (iter trials-remaining trials-passed)
    (cond ((= trials-remaining 0)
           (/ trials-passed trials))
          ((experiment)
           (iter (- trials-remaining 1) (+ trials-passed 1)))
          (else
           (iter (- trials-remaining 1) trials-passed))))
  (iter trials 0))

(define (square x)
  (* x x))

(define unit-circle-contains-random-point?
  (lambda ()
    (let ((xr (pseudo-random-real))
          (yr (pseudo-random-real)))
      (<= (+ (square xr) (square yr)) 1.0))))

; The unit circle is inscribed in a 2x2 square.
(define (estimate-integral P trials)
  (let ((fraction (monte-carlo trials P))
        (square-area 4.0))
    (* fraction square-area)))

; The area of the circle is 𝛑. 
(define (estimate-pi trials)
  (estimate-integral unit-circle-contains-random-point? trials))

(print (estimate-pi 5))
(print (estimate-pi 50))
(print (estimate-pi 500))
(print (estimate-pi 5000))
(print (estimate-pi 50000))
(print (estimate-pi 500000))
