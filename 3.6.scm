
(define rand-init 0)
(define (rand-next state) (+ state 1))

(define rand
  (let ((state rand-init))
    (define (dispatch message)
      (cond ((eq? message 'generate) (begin (set! state (rand-next state))
                                            state))
            ((eq? message 'reset) (lambda (new-value) (set! state new-value)))
            (else (error "Unknown request: " message))))
    dispatch))


(print (rand 'generate))
(print (rand 'generate))
(print "")
((rand 'reset) 0)
(print (rand 'generate))
(print (rand 'generate))
(print (rand 'generate))
(print (rand 'generate))
(print "")
((rand 'reset) 0)
(print (rand 'generate))
(print (rand 'generate))
(print (rand 'generate))
(print (rand 'generate))
