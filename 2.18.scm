; Iterative:
(define (reverse l)
  (define (reverse-iter l r)
    (if (null? l)
        r
        (reverse-iter (cdr l) (cons (car l) r))))
  (reverse-iter l '()))

(reverse (list 1 4 9 16 25))

; Recursive:
(define (reverse l)
  (if (null? l)
      l
      (append (reverse (cdr l)) (list (car l)))))

(reverse (list 1 4 9 16 25))
