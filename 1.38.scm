(define (cont-frac n d k)
  (define (frac i)
    (if (> i k)
        0
        (/ (n i) (+ (d i) (frac (+ i 1))))))
  (trace frac)
  (frac 1))

(define (e k)
  (+ 2 (cont-frac (lambda (i) 1.0)
                  (lambda (i) (if (= (remainder i 3) 2)
                                  (* (/ (+ i 1) 3) 2)
                                  1))
                  k)))

(e 12)
