(import streams)

(define (add-streams s1 s2)
  (stream-map + s1 s2))
(define (mul-streams s1 s2)
  (stream-map * s1 s2))

(define ones (stream-cons 1 ones))
(define integers (stream-cons 1 (add-streams ones integers)))

(define factorials (stream-cons 1 (mul-streams
                                    factorials
                                    (stream-cdr integers))))

(print (stream-ref factorials 0))
(print (stream-ref factorials 1))
(print (stream-ref factorials 2))
(print (stream-ref factorials 3))
(print (stream-ref factorials 4))
(print (stream-ref factorials 5))
(print (stream-ref factorials 6))
