(define (coerce-all args)
  (define (get-coerced-args target-type)
    (map (lambda (arg)
           ((let (coercion (get-coercion (type-tag arg) target-type))
              (if coercion
                  (coercion arg)
                  #f))))
        args))
  (define (all-good? args) (not (memq #f args)))
  (let ((all-types (map type-tag args))))
    (define (try-coercions remaining-types)
      (if (null? remaining-types)
          '()
          (let ((target-type (car remaining-types)))
            (let ((coerced-args (get-coerced-args args target-type)))
              (if (all-good? coerced-args)
                  coerced-args
                  (try-coercions (cdr remaining-types)))))))
    (try-coercions all-types))

; This only works if `op` is defined for all same-type argument lists
; (infinite recursion otherwise).
(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (let ((coerced-args (coerce-all args)))
            (if coerced-args
                (apply-generic op coerced-args)
                (error "No method for these types"
                       (list op type-tags))))))))
