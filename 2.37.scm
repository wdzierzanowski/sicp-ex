(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

(define m (list (list 1 2 3 4) (list 4 5 6 6) (list 6 7 8 9)))
(define n (list (list 1 2 3) (list 4 5 6) (list 6 7 8) (list 8 9 0)))
(define v (list 1 2 3))
(define w (list 2 3 4 5))

(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(dot-product v (cdr w))

(define (matrix-*-vector m v)
  (map (lambda (row) (dot-product row v))
       m))

(matrix-*-vector m w)

(define (transpose mat)
  (accumulate-n cons '() mat))

(transpose m)
(transpose n)

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (row) (matrix-*-vector cols row)) m)))

(matrix-*-matrix m n)
