; 2.83
(define (raise x) (apply-generic 'raise x))

(put 'raise '(integer) (lambda (x) (make-rational x 1)))
(put 'raise '(rational) (lambda (x) (make-real (/ (numer x) (denom x)))))
(put 'raise '(real) (lambda (x) (make-complex-from-real-imag x 0)))
; The type at the top of the tower must always install the identity function
; for `raise`.
(put 'raise '(complex) (lambda (x) x))

; 2.84
(define (successive-raise x target-type)
  (if (equal? (type-tag x) target-type)
      x
      (let ((raised-x (raise x)))
        (if (equal? (type-tag x) (type-tag raised-x))
            #f
            (successive-raise raised-x)))))

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (if (= (length args) 2)
              (let ((type1 (car type-tags))
                    (type2 (cadr type-tags))
                    (a1 (car args))
                    (a2 (cadr args)))
                (if (not (equal? type1 type2))
                    (let ((raised-a1 (successive-raise a1 (type-tag a2)))
                          (raised-a2 (successive-raise a2 (type-tag a1))))
                      (cond (raised-a1 (apply-generic op raised-a1 a2))
                            (raised-a2 (apply-generic op a1 raised-a2))
                            (else (error "No method for these types"
                                         (list op type-tags)))))
                    (error "No method for these types"
                           (list op type-tags)))
              (error "No method for these types"
                     (list op type-tags)))))))
