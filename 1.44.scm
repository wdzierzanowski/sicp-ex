(define (compose f g)
  (lambda (x) (f (g x))))

(define (repeated f n)
  (if (= n 1)
    f
    (compose f (repeated f (- n 1)))))

(define (average a b c)
  (/ (+ a b c) 3))

(define (smooth f)
  (define dx 0.01)
  (lambda (x)
          (average (f (- x dx)) (f x) (f (+ x dx)))))

(define (n-fold-smooth n)
  (repeated smooth n))

(define (sharp x)
  (if (< x 1)
      x
      (- 2 x)))

((smooth sharp) 0)
((smooth sharp) 0.9)
((smooth sharp) 0.995)
((smooth sharp) 1)
((smooth sharp) 1.005)
((smooth sharp) 1.1)
((smooth sharp) 2)

(((n-fold-smooth 5) sharp) 0)
(((n-fold-smooth 5) sharp) 0.9)
(((n-fold-smooth 5) sharp) 0.995)
(((n-fold-smooth 5) sharp) 1)
(((n-fold-smooth 5) sharp) 1.005)
(((n-fold-smooth 5) sharp) 1.1)
(((n-fold-smooth 5) sharp) 2)
