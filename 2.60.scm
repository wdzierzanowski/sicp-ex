(define (element-of-set? x set)
  (cond ((null? set) false)
        ((equal? x (car set)) true)
        (else (element-of-set? x (cdr set)))))

(define (adjoin-set x set)
  (cons x set))

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)        
         (cons (car set1)
               (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))

(element-of-set? 4 '(1 2 4 5 4 2))
(element-of-set? 0 '(1 2 4 5 4 2))
(adjoin-set 4 '(1 2 4 5 4 2))
(adjoin-set 3 '(1 2 4 5 4 2))
(intersection-set '(1 2 3 3 2 3) '(3 0 1 1))

(define (union-set set1 set2)
  (append set1 set2))

(union-set '(1 3 2 3 1) '(3 1 5 5 12 1))

; With- compared to without duplicates: adding elements (including adding by
; union) is cheaper in terms of speed, but more costly in terms of storage.
; Intersections can take longer to process because both traversing `set1` and
; checking `element-of-set? x set2` needs to potentially check duplicate
; elements multple times.
