(define (product term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* result (term a)))))
  (iter a 1))

(define (inc n)
  (+ n 1))

(define (identity n) n)

(define (factorial n)
  (assert (>= n 0))
  (if (= n 0)
      1
      (product identity 1 inc n)))

(factorial 0)
(factorial 1)
(factorial 2)
(factorial 3)
(factorial 10)

; Partial products `num` and `den` exceed the computational range very quickly.
(define (pi-approx n)
  (define (add2 x) (+ x 2))
  (define num
    (square (product identity 4.0 add2 n)))
  (define den
    (square (product identity 3.0 add2 (+ n 1))))
  (* (/ num den) 8 (+ n 2)))

; Multiply fractions instead:
(define (pi-approx n)
  (define (add2 x) (+ x 2))
  (define (term k)
    (/ (* k (+ k 2))
       (square (+ k 1))))
  (* (product term 2.0 add2 n)
     4))

(define max-n 1000000)
(pi-approx max-n)

; For some reason, the following converges more quickly:
(define (wp n)
  (define (term n)
    (* (/ (* 2 n)
          (- (* 2 n) 1))
       (/ (* 2 n)
          (+ (* 2 n) 1))))
  (* (product term 1.0 inc n) 2))

(wp (/ max-n 2))
