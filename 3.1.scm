(define (make-accumulator sum)
  (lambda (x)
    (begin (set! sum (+ x sum))
           sum)))

(define A (make-accumulator 5))
(print (A 10))
(print (A 10))
