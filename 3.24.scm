; First, let's play with the 2D table implementation from the book.
(define (make-table)
  (let ((local-table (list '*table*)))
    (define (lookup key-1 key-2)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (cdr record)
                  #f))
            #f)))
    (define (insert! key-1 key-2 value)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (set-cdr! record value)
                  (set-cdr! subtable
                            (cons (cons key-2 value)
                                  (cdr subtable)))))
            (set-cdr! local-table
                      (cons (list key-1
                                  (cons key-2 value))
                            (cdr local-table)))))
      'ok)    
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
            ((eq? m 'insert-proc!) insert!)
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

(define operation-table (make-table))
(define get (operation-table 'lookup-proc))
(define put (operation-table 'insert-proc!))

(put 'frolic '(real) (lambda (x) (print "Real: " x)))
(put 'frolic '(unreal) (lambda (x) (print "Unreal: " x)))

((get 'frolic '(real)) 12)
((get 'frolic '(unreal)) 13)

; Now on to our customizable table.
(define (make-table-with-pred same-key?)
  (define (custom-assoc key records)
    (cond ((null? records) #f)
          ((same-key? key (caar records)) (car records))
          (else (custom-assoc key (cdr records)))))
  (let ((local-table (list '*table*)))
    (define (lookup key)
      (let ((record (custom-assoc key (cdr local-table))))
        (if record
            (cdr record)
            #f)))
    (define (insert! key value)
      (let ((record (custom-assoc key (cdr local-table))))
        (if record
            (set-cdr! record value)
            (set-cdr! local-table
                      (cons (cons key value) (cdr local-table)))))
      'ok)
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
            ((eq? m 'insert-proc!) insert!)
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

(define custom-table-eq (make-table-with-pred equal?))
((custom-table-eq 'insert-proc!) 100 'a)
((custom-table-eq 'insert-proc!) 200 'b)
((custom-table-eq 'insert-proc!) 300 'c)
(print ((custom-table-eq 'lookup-proc) 321))
(print ((custom-table-eq 'lookup-proc) 198))
(print ((custom-table-eq 'lookup-proc) 140))
(print ((custom-table-eq 'lookup-proc) 20))

(define custom-table-approx (make-table-with-pred
                         (lambda (x y) (< (abs (- x y)) 50))))
((custom-table-approx 'insert-proc!) 100 'a)
((custom-table-approx 'insert-proc!) 200 'b)
((custom-table-approx 'insert-proc!) 300 'c)
(print ((custom-table-approx 'lookup-proc) 321))
(print ((custom-table-approx 'lookup-proc) 198))
(print ((custom-table-approx 'lookup-proc) 140))
(print ((custom-table-approx 'lookup-proc) 20))
