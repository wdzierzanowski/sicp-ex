(define (make-mobile left right)
  (list left right))

(define (make-branch length structure)
  (list length structure))

(define x
  (make-mobile
    (make-branch 1.0
                 (make-mobile
                   (make-branch 2.0 1.0)
                   (make-branch 2.0 1.0)))
    (make-branch 3.0
                 (make-mobile
                    (make-branch 4.0
                                 (make-mobile
                                   (make-branch 5.0 2.0)
                                   (make-branch 5.0 2.0)))
                    (make-branch 6.0
                                 (make-mobile
                                   (make-branch 7.0 3.0)
                                   (make-branch 8.0 3.0)))))))

; a
(define (left-branch mobile) (car mobile))
(define (right-branch mobile) (cadr mobile))
(define (branch-length branch) (car branch))
(define (branch-structure branch) (cadr branch))

; b
(define (total-weight x)
  (if (not (pair? x))
      x
      (+ (total-weight (branch-structure (left-branch x)))
         (total-weight (branch-structure (right-branch x))))))

(total-weight x)

; c
(define (torque branch)
  (* (branch-length branch) (total-weight (branch-structure branch))))
(define (balanced? x)
  (if (not (pair? x))
      #t
      (and (= (torque (left-branch x)) (torque (right-branch x)))
           (balanced? (branch-structure (left-branch x)))
           (balanced? (branch-structure (right-branch x))))))

(balanced? x)

(define b1 (make-mobile
             (make-branch 2.0
                          (make-mobile
                            (make-branch 3.0 1.0)
                            (make-branch 1.0 3.0)))
             (make-branch 2.0 4.0)))

(define b2 (make-mobile
             (make-branch 1.0 b1)
             (make-branch 4.0
                          (make-mobile
                            (make-branch 1.0 1.0)
                            (make-branch 1.0 1.0)))))
(balanced? b1)
(balanced? b2)

; d
; We just need to replace `cadr` with `cdr` in two selectors.
(define (make-mobile left right)
  (cons left right))
(define (make-branch length structure)
  (cons length structure))

(define (right-branch mobile) (cdr mobile))
(define (branch-structure branch) (cdr branch))

(define x
  (make-mobile
    (make-branch 1.0
                 (make-mobile
                   (make-branch 2.0 1.0)
                   (make-branch 2.0 1.0)))
    (make-branch 3.0
                 (make-mobile
                    (make-branch 4.0
                                 (make-mobile
                                   (make-branch 5.0 2.0)
                                   (make-branch 5.0 2.0)))
                    (make-branch 6.0
                                 (make-mobile
                                   (make-branch 7.0 3.0)
                                   (make-branch 8.0 3.0)))))))

(define b1 (make-mobile
             (make-branch 2.0
                          (make-mobile
                            (make-branch 3.0 1.0)
                            (make-branch 1.0 3.0)))
             (make-branch 2.0 4.0)))

(define b2 (make-mobile
             (make-branch 1.0 b1)
             (make-branch 4.0
                          (make-mobile
                            (make-branch 1.0 1.0)
                            (make-branch 1.0 1.0)))))

(total-weight x)
(balanced? x)
(balanced? b1)
(balanced? b2)
