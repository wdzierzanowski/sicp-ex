; Recursive process:
(define (cont-frac n d k)
  (define (frac i)
    (if (> i k)
        0
        (/ (n i) (+ (d i) (frac (+ i 1))))))
  (trace frac)
  (frac 1))

(cont-frac (lambda (i) 1.0)
           (lambda (i) 1.0)
           11)

; Iterative process:
(define (cont-frac n d k)
  (define (frac i partial)
    (if (= i 0)
        partial
        (frac (- i 1) (/ (n i) (+ (d i) partial)))))
  (trace frac)
  (frac k 0))

(cont-frac (lambda (i) 1.0)
           (lambda (i) 1.0)
           11)
