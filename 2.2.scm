(define (print-point p)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

(define (print-segment s)
  (display "(")
  (print-point (start-segment s))
  (display ",")
  (print-point (end-segment s))
  (display ")"))

(define (make-segment p q)
  (cons p q))

(define (start-segment s)
  (car s))

(define (end-segment s)
  (cdr s))

(define (make-point x y)
  (cons x y))

(define (x-point p)
  (car p))

(define (y-point p)
  (cdr p))

(define (midpoint-segment s)
  (define (average a b) (/ (+ a b) 2))
  (let ((p (start-segment s))
        (q (end-segment s)))
    (make-point (average (x-point p) (x-point q))
                (average (y-point p) (y-point q)))))

(define (test-midpoint s)
  (newline)
  (display "The midpoint of ")
  (print-segment s)
  (display " is ")
  (print-point (midpoint-segment s)))

(test-midpoint (make-segment (make-point 0 0) (make-point 2 2)))
(test-midpoint (make-segment (make-point -5 2) (make-point 10 -4)))
