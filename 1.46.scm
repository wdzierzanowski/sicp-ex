(define (iterative-improve good-enough? improve)
  (define (improve-loop guess)
    (if (good-enough? guess)
        guess
        (improve-loop (improve guess))))
  (lambda (guess) (improve-loop guess)))

(define (average x y)
  (/ (+ x y) 2))

(define (sqrt x)
  (define
    sqrt-internal
    (iterative-improve
        (lambda (guess) (< (abs (- (square guess) x)) 0.001))
        (lambda (guess) (average guess (/ x guess)))))
  (sqrt-internal 1.0))

(sqrt 4.0)
(sqrt 16.0)

(define (fixed-point f first-guess)
  (define tolerance 0.00001)
  (define
    fixed-point-internal
    (iterative-improve
        (lambda (guess) (< (abs (- guess (f guess))) tolerance))
        (lambda (guess) (f guess))))
  (fixed-point-internal 1.0))

(fixed-point cos 1.0)
(fixed-point (lambda (y) (+ (sin y) (cos y)))
             1.0)
