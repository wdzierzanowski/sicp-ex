(define (filtered-accumulate filter combiner null-value term a next b)
  (define (filtered a)
    (if (filter a)
        a
        (filtered (next a))))
  (define (iter a result)
    (if (> a b)
        result
        (iter (filtered (next a)) (combiner (term a) result))))
  (iter (filtered a) null-value))

; Somewhat clever use of null-value as a result of filtering, but combiner()
; potentially invoked many times in vain:
(define (filtered-accumulate filter combiner null-value term a next b)
  (define (filtered-term a)
    (if (filter a)
        (term a)
        null-value))
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner (filtered-term a) result))))
  (iter a null-value))

(define (inc n) (+ n 1))

(define (prime? n)
  (= n (smallest-divisor n)))
(define (smallest-divisor n)
  (find-divisor n 2))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))
(define (divides? a b)
  (= (remainder b a) 0))
(define (next a)
  (if (= a 2)
      3
      (+ a 2)))

(define (sum-squares-primes a b)
  (filtered-accumulate prime? + 0 square a inc b))

(sum-squares-primes 4 12)


(define (prod-positive-ints n)
  (define (identity n) n)
  (define (relatively-prime? i)
    (= (gcd i n) 1))
  (filtered-accumulate relatively-prime? * 1 identity 1 inc n))

(prod-positive-ints 16)
